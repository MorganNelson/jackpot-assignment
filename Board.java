public class Board {
	private Die firstDie;
	private Die secondDie;
	private boolean[] tiles;

	public Board(){
		this.firstDie = new Die();
		this.secondDie = new Die();
		this.tiles = new boolean[12];
	}
	public String toString(){
		String tilesInString="";
		for(int i=0;i<this.tiles.length;i++){
			if(!this.tiles[i]){
				tilesInString += ""+i+" ";
			}else{
				tilesInString += "X ";
			}
		}
		return tilesInString;
	}
	public boolean playATurn(){
		firstDie.roll();
		secondDie.roll();
		int sumOfDice = this.firstDie.getFaceValue() + this.secondDie.getFaceValue();
		System.out.println("first die: " +firstDie.toString() + ", second die " + secondDie.toString());
		if(!this.tiles[sumOfDice-1]){
			this.tiles[sumOfDice-1] = true;
			// Do I really need all these this.?
			System.out.println("Closing tile equal to sum");
			return false;
		}else if(!this.tiles[firstDie.getFaceValue()-1]){
			this.tiles[firstDie.getFaceValue()-1] = true;
			System.out.println("Closing tile equal to sum of die 1");
			return false;
		}else if(!this.tiles[secondDie.getFaceValue()-1]){
			this.tiles[secondDie.getFaceValue()-1]=true;
			System.out.println("Closing tile equal to sum of die 2");
			return false;
		}else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}