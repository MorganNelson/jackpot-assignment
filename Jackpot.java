	public class Jackpot{
		public static void main(String[]args){
			/*Die testDie = new Die();
			System.out.println(testDie.getFaceValue());
			testDie.roll();
			System.out.println(testDie.getFaceValue());
			testDie.roll();
			System.out.println(testDie.getFaceValue());
			testDie.roll();
			System.out.println(testDie.getFaceValue());
			System.out.println("In string now");
			System.out.println(testDie.toString());
			//Testing Die class, no error here
			Board testBoard = new Board();
			System.out.println(testBoard.toString());
			System.out.println(testBoard.playATurn());
			//Testing board class, no error here
			*/
			System.out.println("Welcome to the the lobby, please have a seat");
			Board realBoard = new Board();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
			while(gameOver!=true){
				System.out.println(realBoard.toString());
				if(realBoard.playATurn()){
					gameOver = true;
				}else{
					numOfTilesClosed ++;
				}
			}
			if(numOfTilesClosed>=7){
				System.out.println("You reached the jackpot!");
			}else{
				System.out.println("You lost!");
			}
		}
	}
