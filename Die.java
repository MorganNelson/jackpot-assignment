import java.util.Random;
public class Die{
	private int faceValue;
	private Random random = new Random();

	public Die(){
		this.faceValue=1;
	}
	public int getFaceValue(){
		return this.faceValue;
	}
	public void roll(){
		faceValue = random.nextInt(6)+1;
	}
	public String toString(){
		return "" +this.faceValue +"";
	}
}